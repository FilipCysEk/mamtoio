$("#register-form").submit(validateRegisterForm);

if(registerUserExist == 1){
    invalidInputTxt($("#register-part-email").get(), "Podany użytkownik istnieje");
}

function validateRegisterForm(){
    let errC = 0;
    let email = $("#register-part-email").get();
    let emailRepeat = $("#register-part-email-repeat").get();

    let pass = $("#register-part-password").get();
    let passRepeat = $("#register-part-password-repeat").get();

    let nameIn = $("#register-part-username").get();

    if($(email).val().length > 50){
        errC++;
        invalidInputTxt(email, "Email jest zbyt długi")
    }
    else{
        deleteInputFeedback(email);
    }

    if(validateEmail($(email).val())){
        deleteInputFeedback(email);
    }
    else{
        errC++;
        invalidInputTxt(email, "Składnia adresu email jest niepoprawna")
    }

    if($(email).val() == $(emailRepeat).val()){
        deleteInputFeedback(email);
        deleteInputFeedback(emailRepeat);
        if(errC === 0){
            validInput(email);
            validInput(emailRepeat);
        }
    }
    else{
        errC++;
        invalidInputTxt(email, "Adresy nie są takie same");
        invalidInput(emailRepeat);
    }


    if($(pass).val().length < 6){
        errC++;
        invalidInputTxt(pass, "Hasło jest zbyt krótkie");
    }
    else{
        deleteInputFeedback(pass);
    }

    if(isInPasswordBigLetters($(pass).val(), 1)){
        deleteInputFeedback(pass);
    }
    else{
        errC++;
        invalidInputTxt(pass, "W haśle nie ma wielkich liter.");
    }

    if(isInPasswordNumbers($(pass).val(), 1)){
        deleteInputFeedback(pass);
    }
    else{
        errC++;
        invalidInputTxt(pass, "W haśle nie ma żadnej cyfry.");
    }

    if($(pass).val() == $(passRepeat).val()){
        if(errC === 0) {
            validInput(pass);
            validInput(passRepeat);
        }
    }
    else{
        errC++;
        invalidInputTxt(pass, "Hasła nie są identyczne");
        invalidInput(passRepeat);
    }

    if($(nameIn).val().length > 3){
        if(errC === 0) {
            validInput(nameIn);
        }
        else
            deleteInputFeedback(nameIn);
    }
    else{
        errC++;
        invalidInputTxt(nameIn, "Nazwa użytkownika zbyt krótka");
    }

    return errC === 0;
}