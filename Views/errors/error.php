<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error page</title>
    <?php /** @var string $error_info */
    /** @var string $error_admin_info */?>
</head>
<body>
<h1>Błąd!!</h1>
<h2><?= $error_info?></h2>
<h3><?= $error_admin_info?></h3>
<a href="?">Powrót do strony głównej</a>
</body>
</html>