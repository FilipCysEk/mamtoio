var idCatToDelete;
var processedCategory;
var processedSubcategory;

function deleteCategoryClick(elem) {
    idCatToDelete = elem.dataset.idcategory;
    getCategoryListWOutToDelete();
    //console.log(idCatToDelete);
}

function deleteCategoryExecute() {
    let idsub = $("#delete-subcategory-list option:selected").val()
    if(idsub == "" || idsub == undefined)
        alert("Nie można usunąć tej kategorii!!");
    else
        window.location.href = "?page=settingsDeleteCategory&id_cat=" + idCatToDelete + "&id_subcat=" + idsub;
}

function updateProcessedCategory(id) {
    processedCategory = id;
}

function addSubcategory(elem) {
    let temp = elem;
    temp = temp.parentElement;
    temp = temp.getElementsByTagName("input")[0];
    let name = temp.value;
    let id_cat = elem.dataset.idcategory;

    if (name.length < 3) {
        invalidInput(temp);
        return false
    } else {
        temp.classList.remove('is-invalid')
    }

    $.ajax({
        type: "POST",
        url: "?page=ajaxAddSubCategory",
        data: {id_cat: id_cat, name: name},
        success: function (json) {
            temp.value = "";
            updateOpenedSubcategoryList(id_cat)
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function invalidInput(elem) {
    let tab = Array.from(elem.classList);
    if (tab.indexOf("is-invalid") == -1) {
        elem.classList.add("is-invalid");
    }
}

function updateOpenedSubcategoryList(id_cat) {
    $.ajax({
        type: "POST",
        url: "?page=ajaxGetSubcategoryList",
        data: {id_sub: id_cat},
        success: function (json) {
            //json = $.parseJSON(json);

            subcategoryListRedraw(json, $("#list-acc-content-id-" + id_cat));
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function subcategoryListRedraw(tab, parent) {
    let htmlList = parent.find(".list-group").get();
    let temp;


    let thunb = $(parent).find(".item-thunbail").get(0);
    thunb = Array($(thunb).text(), $(thunb).css("background-color"));
    thunb[0] = thunb[0].replace("\n", "");
    thunb[0] = thunb[0].replace(" ", "");

    $(htmlList).empty();
    temp = $("<div></div>").addClass('list-group-item list-group-item-action active disabled');
    $(htmlList).append(temp);
    let defSubcat = $(temp).get();
    temp = $("<div></div>").addClass("d-flex");
    $(defSubcat).append(temp);
    let defSubcatDflex = $(temp).get();
    $(defSubcatDflex).append($("<h4></h4>").addClass("item-right-part mt-2 pb-2").text(tab[0][1]));
    temp = $("<button></button>").addClass("btn btn-warning ml-auto");
    $(temp).attr({
        "onclick": "editCategoryName(this)", "data-target": "#edit-category-name-window",
        "data-toggle": "modal", "data-idsubcategory": tab[0]['id_sub_cat']
    });
    $(defSubcatDflex).append(temp);
    let button = $(temp).get();
    $(button).append($("<i></i>").addClass("material-icons align-middle").text("edit"));
    $(temp).html($(temp).html() + " Edytuj");

    let tabSize = tab.length;
    for (let i = 1; i < tabSize; i++) {
        temp = $("<div></div>").addClass("list-group-item list-group-item-action");
        $(htmlList).append(temp);
        let subRow = temp.get();

        temp = $("<div></div>").addClass("d-flex");
        $(subRow).append(temp);
        let subRowDflex = temp.get();

        temp = $("<div></div>").addClass("item-thunbail align-self-center");
        temp.css("background", tab[i]['color']);
        temp.text(thunb[0]);
        $(subRowDflex).append(temp);

        $(subRowDflex).append($("<h5></h5>").addClass("item-right-part mt-1").text(tab[i][1]));

        //Button
        temp = $("<button></button>").addClass("btn btn-warning ml-auto");
        $(temp).attr({
            "data-toggle": "modal",
            "data-target": "#edit-subcategory-window",
            "data-idsubcategory": tab[i]['id_sub_cat'],
            "onclick": "editSubCategoryNameClick(this)"
        });
        $(subRowDflex).append(temp);
        let button = $(temp).get();
        $(button).append($("<i></i>").addClass("material-icons align-middle").text("edit"));
        $(temp).html($(temp).html() + " Edytuj");
    }

    temp = $("<div></div>").addClass("list-group-item list-group-item-action");
    $(htmlList).append(temp);
    let newSub = $(temp).get();
    temp = $("<div></div>").addClass("d-flex");
    $(newSub).append(temp);
    newSub = $(temp).get();

    temp = $("<div></div>").addClass("item-thunbail align-self-center");
    temp.css("background", tab[0]['color']);
    temp.text(thunb[0]);
    $(newSub).append(temp);

    temp = $("<input>").addClass("mt-1 form-control ml-3 mr-5")
        .attr({"type": "text", "name": "newSubcat", "placeholder": "Nowa podkategoria", "pattern": ".{3,20}"});
    $(newSub).append(temp);

    temp = $("<button></button>").addClass("btn btn-success ml-auto")
        .attr({"data-idcategory": tab[0]['id_category'], "onclick": "addSubcategory(this)"}).text("Dodaj");
    $(newSub).append(temp);
}

function editSubCategoryNameClick(elem) {
    processedSubcategory = $(elem).data("idsubcategory");
    let name = $(elem).prev().get();
    name = $(name).text();

    let modalContent = $("#modal-body-subcategory-edit").get();
    let input = $(modalContent).find("input").get();
    $(input).val(name);
}

function editSubCategoryExecute() {
    let id_sub_cat = processedSubcategory;
    let modalContent = $("#modal-body-subcategory-edit").get();
    let input = $(modalContent).find("input").get();
    let name = $(input).val();

    $.ajax({
        type: "POST",
        url: "?page=ajaxUpdateSubcategoryName",
        data: {id_sub: id_sub_cat, name: name},
        success: function (json) {
            updateOpenedSubcategoryList(processedCategory);
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function editCategoryName(elem) {
    processedSubcategory = $(elem).data("idsubcategory");
    let name = $(elem).prev().get();
    name = $(name).text();
    name = name.substr(10);
    console.log(name);

    let modalContent = $("#modal-body-category-edit").get();
    let input = $(modalContent).find("input").get();
    $(input).val(name);
}

function editCategoryExecute() {
    let id_sub_cat = processedSubcategory;
    let modalContent = $("#modal-body-category-edit").get();
    let input = $(modalContent).find("input").get();
    let name = $(input).val();

    window.location.href = "?page=ajaxUpdateCategoryName&id_sub=" + id_sub_cat + "&name=" + name;
}

function deleteSubcategoryExecute() {
    let id_sub_cat = processedSubcategory;

    $.ajax({
        type: "POST",
        url: "?page=ajaxDeleteSubcategory",
        data: {id_sub: id_sub_cat},
        success: function (json) {
            updateOpenedSubcategoryList(processedCategory);
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function categoryToExpense(elem) {
    let id_cat = processedCategory;

    $.ajax({
        type: "POST",
        url: "?page=ajaxCategoryToExpense",
        data: {id_cat: id_cat},
        success: function (json) {
            let but = $(elem).parent().prev().get();
            $(but).text("Wydatek ");
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function categoryToIncome(elem) {
    let id_cat = processedCategory;

    $.ajax({
        type: "POST",
        url: "?page=ajaxCategoryToIncome",
        data: {id_cat: id_cat},
        success: function (json) {
            let but = $(elem).parent().prev().get();
            $(but).text("Przychód ");
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function updateColor(elem) {
    let id_cat = processedCategory;

    let listItem = $(elem).find(".dropdown-color-div").get();
    let color = $(listItem).css("background-color");
    color = rgb2hex(color);

    $.ajax({
        type: "POST",
        url: "?page=ajaxUpdateColor",
        data: {id_cat: id_cat, color: color},
        success: function (json) {

            let but = $(elem).parent().prev();
            but = $(but).find(".dropdown-color-div").get();
            $(but).css("background-color", color);
            updateOpenedSubcategoryList(processedCategory);
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function getCategoryListWOutToDelete(){
    let id_cat = processedCategory;
    $.ajax({
        type: "POST",
        url: "?page=ajaxGetCategoryListWOutDeleted",
        data: {id_cat: id_cat},
        success: function (json) {
            updateCategoryDeleteRender(json);
        },
        error: function (blad) {
            alert("Błąd");
            console.log(blad);
        }
    });
}

function updateCategoryDeleteRender(json){
    //json = $.parseJSON(json);
    $("#delete-category-list").children("option").remove();
    let size = json.length;
    for(let i = 0; i < size; i++){
        $("#delete-category-list")
            .append($("<option></option>")
                .attr("value", json[i]['id_category'])
                .text(json[i]['name']));
    }
    updateSubcategoryDelete(document.getElementById("delete-category-list"));
}

function updateSubcategoryDelete(elem) {
    if (elem.options.length > 0) {
        let val = elem.options[elem.selectedIndex].value;

        $.ajax({
            type: "POST",
            url: "?page=ajaxGetSubcategoryList",
            data: {id_sub: val},
            success: function (json) {
                //json = $.parseJSON(json);
                $("#delete-subcategory-list").children("option").remove();
                let size = json.length;
                for(let i = 0; i < size; i++){
                    $("#delete-subcategory-list")
                        .append($("<option></option>")
                            .attr("value", json[i]['id_sub_cat'])
                            .text(json[i]['name']));
                }
            },
            error: function (blad) {
                alert("Błąd");
                console.log(blad);
            }
        });
    }
    else
    {
        $("#delete-subcategory-list").children("option").remove();
    }
    return false;
}












let hexDigits = Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");

//Function to convert rgb color to hex format
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}