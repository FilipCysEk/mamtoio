<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 23.10.2018
 * Time: 08:27
 */

namespace controllers;


class AppController
{
    private $request = null;

    protected $error_codes = [
        1 => "Błąd aplikacji",
        2 => "Błąd połączenia z bazą danych",
        3 => "Błąd w bazie danych",
        4 => "Błąd przesłanych danych"
    ];

    public function __construct()
    {
        $this->request = strtoupper($_SERVER['REQUEST_METHOD']);

    }

    protected function isGet()
    {
        return $this->request == 'GET';
    }

    protected function isPost()
    {
        return $this->request == 'POST';
    }

    protected function getCookie($cookie_name){
        if(!ISSET($_COOKIE[$cookie_name]) || EMPTY($_COOKIE[$cookie_name]))
            return -1;
        else
            return $_COOKIE[$cookie_name];
    }

    protected function getUserId(){
        if(isset($_COOKIE['user_id']))
            return $_COOKIE['user_id'];
        elseif (isset($_SESSION['user_id']))
            return $_SESSION['user_id'];
        else
            return -1;
    }

    protected function extendCookieLife($cookie_name){
        if($this->getCookie($cookie_name) != -1) {
            setcookie($cookie_name, $_COOKIE[$cookie_name], time() + 3600 * 24 * 7);
        }
    }

    protected function isLogedin(){
        if(isset($_COOKIE['user_id']) && $_COOKIE['user_id'] > 0 || isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0)
            return true;
        else
            return false;
    }

    protected function isLogedinAndMove(){
        if($this->isLogedIn())
            return true;
        else
            header("Location:?page=index");
        exit();
    }

    protected function render(string $filename = null, array $variables = [])
    {
        $this->extendCookieLife("user_id");
        $class = get_class($this);
        $class = (substr($class, strrpos($class, '\\') + 1));
        $view = $filename ? dirname(__DIR__).'/views/'.$class.'/'.$filename.'.php' : '';

        $newExpModel = new NewExpense();
        $variables['newExpense'] = $newExpModel->getContentNewExpenseWindow($this->getUserId());

        $output = "There isn't such file to open";

        if(file_exists($view)){
            extract($variables);

            ob_start();
            include($view);
            $output = ob_get_clean();
        }

        print($output);
    }

    protected function checkBigLettersInString($str, $enough)
    {
        $noBigCount = strlen(preg_replace('![^A-Z]+!', '', $str));

        if ($noBigCount < $enough)
            return false;
        else
            return true;
    }

    protected function checkBigNoumbersInString($str, $enough)
    {
        $noBigCount = strlen(preg_replace('![^0-9]+!', '', $str));

        if ($noBigCount < $enough)
            return false;
        else
            return true;
    }

    protected function jsonReturnError($txt = false) {
        http_response_code(500);
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($txt);
    }

    protected function jsonReturnSuccess($txt = true){
        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode($txt);
    }

    public function throwErrorPage($error_code, $error_admin = null){
        if($error_admin == null){
            header("Location:?page=error&error_code=$error_code");
        }
        else
        {
            $txt = "Location:?page=error&error_code=$error_code&error_admin=$error_admin";
            $txt = str_replace("\n", "", $txt);
            header($txt);
        }
        exit();
    }

}