<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 20.11.2018
 * Time: 08:40
 */

namespace model;

use PDO;
use PDOException;

require_once "./Config/database_params.php";

class Database
{
    private $host;
    private $username;
    private $password;
    private $database;

    /**
     * database constructor.
     * @param
     */
    public function __construct()
    {
        $this->host = HOST;
        $this->username = USERNAME;
        $this->password = PASSWORD;
        $this->database = USERNAME;
    }

    protected function connect()
    {
        try {
            return new PDO("mysql:host=$this->host;dbname=$this->database", $this->username, $this->password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'", PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (PDOException $e) {
            $eadmin = ADMIN_ERROR_INFO ? $e->getMessage() : '';
            header("Location:?page=error&error_code=2&error_admin=$eadmin");
            exit();
        }
    }

    protected function returnOnlyOneResult($res, $colName){
        if(count($res) == 1)
            return $res[0][$colName];

        return -1;
    }

    protected function tabPrefix($name){
        return NAME_PREFIX . $name;
    }


}