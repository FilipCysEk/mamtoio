<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 11.12.2018
 * Time: 10:47
 */

namespace controllers;

use Model\Expenses;

class NewExpense extends AppController
{
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Expenses();
    }

    public function getContentNewExpenseWindow($id_user)
    {
        if ($id_user != -1) {
            $returnedArray = [];

            $returnedArray["account_list"] = $this->model->getAccountsList($id_user);
            $returnedArray["category_list"] = $this->model->getCategoryListExpense($id_user);

            return $returnedArray;
        } else
            return [];
    }

    public function addItem()
    {
        $res = null;
        switch ($_POST['type-item']) {
            case "income":
                if ($this->checkNewItemData())
                    $res = $this->addIncome();
                else {
                    header("Location:?page=error&error_code=4&error_admin=items_error");
                    exit();
                }
                break;
            case "expense":
                if ($this->checkNewItemData())
                    $res = $this->addExpense();
                else {
                    header("Location:?page=error&error_code=4&error_admin=items_error");
                    exit();
                }
                break;
            case "transaction":
                if ($this->checkNewTransactionData())
                    $res = $this->addTransaction();
                else {
                    header("Location:?page=error&error_code=4&error_admin=items_error");
                    exit();
                }
                break;
            default:
                header("Location:?page=error&error_code=4&error_admin=bad_type");
                exit();
        }

        if ($res)
            header("Location:?page=dashboard");
        else
            $this->throwErrorPage(3, "Error when adding new item in to expense table");

        exit();
    }

    public function ajaxAddItem()
    {
        $res = null;
        switch ($_POST['type-item']) {
            case "income":
                if ($this->checkNewItemData())
                    $res = $this->addIncome();
                else
                    $this->jsonReturnError("Items_error");

                break;
            case "expense":
                if ($this->checkNewItemData())
                    $res = $this->addExpense();
                else
                    $this->jsonReturnError("Items_error");

                break;
            case "transaction":
                if ($this->checkNewTransactionData())
                    $res = $this->addTransaction();
                else
                    $this->jsonReturnError("Items_error");

                break;
            default:
                $this->jsonReturnError("Bad-type");
        }

        if ($res)
            $this->jsonReturnSuccess();
        else
            $this->jsonReturnError();
    }


    private function addIncome()
    {
        if ($_POST["money-item"] < 0)
            $_POST["money-item"] *= -1;

        $note = isset($_POST["description-item"]) ? $_POST["description-item"] : "";

        return ($this->model->addExpense($_POST["acc-item"], $_POST["money-item"], $_POST["date-item"], $note,
            $_POST["subcat-item"], 1));
    }

    private function addExpense()
    {
        if ($_POST["money-item"] > 0)
            $_POST["money-item"] *= -1;

        $note = isset($_POST["description-item"]) ? $_POST["description-item"] : "";

        return ($this->model->addExpense($_POST["acc-item"], $_POST["money-item"], $_POST["date-item"], $note,
            $_POST["subcat-item"], 0));

    }

    private function addTransaction()
    {
        if ($_POST["money-item"] < 0)
            $_POST["money-item"] *= -1;

        return ($this->model->addTransfer($_POST["acc-source"], $_POST["acc-dest"], $_POST["money-item"], $_POST["date-item"]));

    }

    private function checkNewItemData()
    {

        if (!ISSET($_POST['type-item']) || (empty($_POST['type-item']) === ""))
            return false;

        if (!ISSET($_POST['acc-item']) || (empty($_POST['acc-item']) === ""))
            return false;

        if (!ISSET($_POST['money-item']) || (empty($_POST['money-item']) === ""))
            return false;

        if (!ISSET($_POST['cat-item']) || (empty($_POST['cat-item']) === ""))
            return false;

        if (!ISSET($_POST['subcat-item']) || (empty($_POST['subcat-item']) === ""))
            return false;

        if (!ISSET($_POST['date-item']) || (empty($_POST['date-item']) === ""))
            return false;

        return true;
    }

    private function checkNewTransactionData()
    {
        if (!ISSET($_POST['type-item']) || (empty($_POST['type-item']) === ""))
            return false;

        if (!ISSET($_POST['acc-source']) || (empty($_POST['acc-source']) === ""))
            return false;

        if (!ISSET($_POST['acc-dest']) || (empty($_POST['acc-dest']) === ""))
            return false;

        if ($_POST['acc-source'] == $_POST['acc-dest'])
            return false;

        if (!ISSET($_POST['money-item']) || (empty($_POST['money-item']) === ""))
            return false;

        if (!ISSET($_POST['date-item']) || (empty($_POST['date-item']) === ""))
            return false;

        return true;
    }

    public function ajaxGetSubcategories()
    {
        if ($this->isPost()) {

            $matherCategory = $_POST['id_sub'];
            $res = $this->model->getSubCategoryList($matherCategory);

            if (!$res || empty($res)) {
                $res = "";
            }
            //print_r($res);

            $res2 = Array();
            foreach ($res as $val) {
                $res2[$val["id_sub_cat"]] = $val['name'];
            }
            $this->jsonReturnSuccess($res);
        } else
            $this->jsonReturnError();
    }

    public function ajaxGetCategoriesExpenses()
    {
        if (($user_id = $this->getUserId()) != -1) {
            $res = $this->model->getCategoryListExpense($user_id);

            if (!$res || empty($res)) {
                $res2 = "";
            } else {
                //print_r($res);

                $res2 = Array();
                foreach ($res as $val) {
                    $res2[$val["id_category"]] = $val['name'];
                }
            }
            $this->jsonReturnSuccess($res2);
        } else
            $this->jsonReturnError();
    }

    public function ajaxGetCategoriesIncomes()
    {
        if (($user_id = $this->getUserId()) != -1) {
            $res = $this->model->getCategoryListIncome($user_id);

            if (!$res || empty($res)) {
                $res2 = null;
            } else {
                //print_r($res);

                $res2 = Array();
                foreach ($res as $val) {
                    $res2[$val["id_category"]] = $val['name'];
                }
            }
            $this->jsonReturnSuccess($res2);
        } else
            $this->jsonReturnError();
    }


}