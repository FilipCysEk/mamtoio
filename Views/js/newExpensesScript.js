updateSubcategory(document.getElementById("modal-new-item-cat-item"));

$('#new-expense-window-btn-add').click(addNewExpenseAndClose);
$('#new-expense-window-btn-addNext').click(addNewExpenseAndNext);

function updateSubcategory(elem) {
    if (elem.options.length > 0) {
        var val = elem.options[elem.selectedIndex].value;

        $.ajax({
            type: "POST",
            url: "?page=ajaxGetSubcategoryList",
            data: {id_sub: val},
            success: function (json) {
                //console.log(json);
                //alert("success");
                //json = $.parseJSON(json);
                $("#modal-new-item-subcat-item").children("option").remove();
                $.each(json, function (key, value) {

                    $("#modal-new-item-subcat-item")
                        .append($("<option></option>")
                            .attr("value", value['id_sub_cat'])
                            .text(value['name']));
                });
            },
            error: function (blad) {
                alert("Błąd");
                console.log("Update Subcategory error");
                console.log(blad);
            }
        });
    }
    return false;
}

function refreshCategories(){
    setDefDate();
    updateCategory(document.getElementById("modal-new-item-type-item"));
}

function updateCategory(elem) {
    var val = elem.options[elem.selectedIndex].value;

    if( val == "income") {
        $.ajax({
            type: "POST",
            url: "?page=ajaxGetCategoryIncomeList",
            success: function (json) {
                updateCategoryRender(json);
            },
            error: function (blad) {
                alert("Błąd");
                console.log("Update category (income) error");
                console.log(blad);
            }
        });
    }
    else if( val == "expense"){
        $.ajax({
            type: "POST",
            url: "?page=ajaxGetCategoryExpenseList",
            success: function (json) {
                updateCategoryRender(json);
            },
            error: function (blad) {
                alert("Błąd");
                console.log("Update category (expense) error");
                console.log(blad);
            }
        });
    }

    //console.log($("#modal-new-item-cat-item").children("option").text());
    //setTimeout(updateSubacategoryRun, 200);

    return false;
}

function updateCategoryRender(json){
    //console.log(json);
    //alert("success");
    //json = $.parseJSON(json);
    $("#modal-new-item-cat-item").children("option").remove();
    $.each(json, function (key, value) {

        $("#modal-new-item-cat-item")
            .append($("<option></option>")
                .attr("value", key)
                .text(value));
    });
    updateSubcategory(document.getElementById("modal-new-item-cat-item"));
}

/*
    Inserting new item
 */

function addNewExpenseAndClose(){
    insertNewItemIntoDB(addNewExpenseAndCloseAfter)

}

function addNewExpenseAndCloseAfter(){
    $('#modal-new-item').modal('hide');
    clearContentNewExpenseModal();
    refreshContentPage();
}

function addNewExpenseAndNext(){
    insertNewItemIntoDB(addNewExpenseAndNextAfter);
}

function addNewExpenseAndNextAfter(){
    clearContentNewExpenseModal();
    refreshContentPage();
}

/*
    Inserting new item to DB
 */

function insertNewItemIntoDB(funcOnSuccess){
    let type = $('#modal-new-item-type-item').val();
    if(type == undefined)
        type = $('#modal-new-item-type-item2').val();

    if(type == "income" || type == "expense")
        insertNewItemIntoDBIncomeExpense(type, funcOnSuccess);
    else if(type == "transaction")
        insertNewItemIntoDBTransaction(type, funcOnSuccess)

}

function insertNewItemIntoDBIncomeExpense(type, funcOnSuccess){
    if(validateIncomeExpense()) {
        $.ajax({
            type: "POST",
            url: "?page=ajaxAddNewItem",
            data: {
                'type-item': type,
                'acc-item': $('#modal-new-item-acc-item').val(),
                'money-item': $('#modal-new-item-money-item').val(),
                'description-item': $('#modal-new-item-description-item').val(),
                'cat-item': $('#modal-new-item-cat-item').val(),
                'subcat-item': $('#modal-new-item-subcat-item').val(),
                'date-item': $('#modal-new-item-date-item').val()
            },
            success: function (json) {
                funcOnSuccess();
            },
            error: function (json) {
                alert("Błąd");
                console.log("Error when inserting new data");
                console.log(json)
            }
        });
    }
}

function insertNewItemIntoDBTransaction(type, funcOnSuccess){
    if(validateTransaction()) {
        $.ajax({
            type: "POST",
            url: "?page=ajaxAddNewItem",
            data: {
                'type-item': type,
                'money-item': $('#modal-new-item-money-item').val(),
                'acc-source': $('#modal-new-item-acc-source').val(),
                'acc-dest': $('#modal-new-item-acc-dest').val(),
                'date-item': $('#modal-new-item-date-item').val()
            },
            success: function (json) {
                funcOnSuccess();
            },
            error: function (json) {
                alert("Błąd");
                console.log("Error when inserting new data");
                console.log(json)
            }
        });
    }
}

/*
    Modal functions
 */

function clearContentNewExpenseModal(){
    $('#modal-new-item-money-item').val("");
    //$('#modal-new-item-acc-source').val("");
    //$('#modal-new-item-acc-dest').val("");
    $('#modal-new-item-date-item').val("");
    //$('#modal-new-item-acc-item').val("");
    $('#modal-new-item-description-item').val("");
    //$('#modal-new-item-cat-item').val("");
    //$('#modal-new-item-subcat-item').val("");
    setDefDate();
}

function refreshContentPage(){
    let href = window.location.href;

    if(href.indexOf("dashboard") !== -1){
        getLast10Items();
    }
    else if (href.indexOf("expenseList") !== -1)
        filterExpenses();

}

/*
    Validation
 */

function validateTransaction(){
    if($("#modal-new-item-acc-source").val() == $("#modal-new-item-acc-dest").val()){
        invalidInputTxt($("#modal-new-item-acc-source").get(), "Konta nie mogą być takie same");
        invalidInput($("#modal-new-item-acc-dest").get());
        return false;
    }
    else{
        deleteInputFeedback($("#modal-new-item-acc-source").get());
        deleteInputFeedback($("#modal-new-item-acc-dest").get());
    }

    let now = new Date();
    now = now.getTime();

    let dateForm = $('#modal-new-item-date-item').val();
    dateForm = new Date(dateForm);
    dateForm = dateForm.getTime();

    if(now < dateForm) {
        invalidInputTxt($('#modal-new-item-date-item').get(), "Nie można podawać daty z przyszłości");
        return false;
    }
    else{
        deleteInputFeedback($("#modal-new-item-date-item").get());
    }

    return true;
}

function validateIncomeExpense(){
    let now = new Date();
    now = now.getTime();

    let dateForm = $('#modal-new-item-date-item').val();
    dateForm = new Date(dateForm);
    dateForm = dateForm.getTime();

    if(now < dateForm) {
        invalidInputTxt($('#modal-new-item-date-item').get(), "Nie można podawać daty z przyszłości");
        return false;
    }
    else{
        deleteInputFeedback($("#modal-new-item-date-item").get());
    }

    return true;
}

