<?php

class Routing
{
    public $routes = array();

    public function __construct()
    {
        //$class = new controllers\DefaultController();
        $this->routes = [
            'index' => [
                'controller' => 'DefaultController',
                'action' => 'index'
            ],
            'no_page' => [
                'controller' => 'DefaultController',
                'action' => 'no_page'
            ],
            'error' => [
                'controller' => 'DefaultController',
                'action' => 'error'
            ],
            'login' => [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'DefaultController',
                'action' => 'logout'
            ],
            'dashboard' => [
                'controller' => 'Dashboard',
                'action' => 'index'
            ],
            'addNewExpense' => [
                'controller' => 'NewExpense',
                'action' => 'addItem'
            ],
            'ajaxGetSubcategoryList' => [
                'controller' => 'NewExpense',
                'action' => 'ajaxGetSubcategories'
            ],
            'ajaxGetCategoryExpenseList' => [
                'controller' => 'NewExpense',
                'action' => 'ajaxGetCategoriesExpenses'
            ],
            'ajaxGetCategoryIncomeList' => [
                'controller' => 'NewExpense',
                'action' => 'ajaxGetCategoriesIncomes'
            ],
            'settings' => [
                'controller' => 'Settings',
                'action' => 'index'
            ],
            'settingsCatEditor' => [
                'controller' => 'Settings',
                'action' => 'categoryEditor'
            ],
            'settingsNewCategory' => [
                'controller' => 'Settings',
                'action' => 'newCategory'
            ],
            'settingsDeleteCategory' => [
                'controller' => 'Settings',
                'action' => 'deleteCategory'
            ],
            'ajaxAddSubCategory' => [
                'controller' => 'Settings',
                'action' => 'addSubcategory'
            ],
            'ajaxUpdateSubcategoryName' => [
                'controller' => 'Settings',
                'action' => 'editSubcategoryName'
            ],
            'ajaxUpdateCategoryName' => [
                'controller' => 'Settings',
                'action' => 'editCategoryName'
            ],
            'ajaxDeleteSubcategory' => [
                'controller' => 'Settings',
                'action' => 'deleteSubcategory'
            ],
            'ajaxCategoryToIncome' => [
                'controller' => 'Settings',
                'action' => 'categoryToIncome'
            ],
            'ajaxCategoryToExpense' => [
                'controller' => 'Settings',
                'action' => 'categoryToExpense'
            ],
            'ajaxUpdateColor' => [
                'controller' => 'Settings',
                'action' => 'categoryUpdateColor'
            ],
            'ajaxGetCategoryListWOutDeleted' => [
                'controller' => 'Settings',
                'action' => 'getCategoryListWOutDeleted'
            ],
            'changePassword' => [
                'controller' => 'Settings',
                'action' => 'changePassword'
            ],
            'deleteMoneyAccount' => [
                'controller' => 'Settings',
                'action' => 'deleteMoneyAccount'
            ],
            'addMoneyAccount' => [
                'controller' => 'Settings',
                'action' => 'ajaxAddMoneyAccount'
            ],
            'updateMoneyAccount' => [
                'controller' => 'Settings',
                'action' => 'ajaxUpdateNameMoneyAccount'
            ],
            'registerUser' => [
                'controller' => 'DefaultController',
                'action' => 'createNewUser'
            ],
            'deleteAccount' => [
                'controller' => 'Settings',
                'action' => 'deleteUserAccount'
            ],
            'ajaxAddNewItem' => [
                'controller' => 'NewExpense',
                'action' => 'ajaxAddItem'
            ],
            'expenseList' => [
                'controller' => 'ExpenseList',
                'action' => 'index'
            ],
            'ajaxFilterExpenses' => [
                'controller' => 'ExpenseList',
                'action' => 'ajaxFilterExpenses'
            ],
            'ajaxGetLast10Item' => [
                'controller' => 'ExpenseList',
                'action' => 'ajaxGetLast10Item'
            ]
        ];
    }

    public function run()
    {
        if (isset($_GET['page']))
            $page = isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'no_page';
        else
            $page = 'index';

        if ($this->routes[$page]) {
            $class = 'controllers\\' . $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            try {
                $object = new $class;
                $object->$action();
            } catch (Exception $e) {
                echo $e;
            }
        }
    }
}