<!doctype html>
<html lang="pl" class="max-window-height">
<head>
    <?php /** @var array $variables */
    require_once("Views/blocks/head.html"); ?>
</head>
<body id="white-background" class="max-window-height">
<?php
require_once "Views\blocks\leftmenu.html"; ?>

<aside class="right-aside">

    <h3 class="col-12 text-center right-aside-title">Filtrowanie</h3>
    <div class="expense-list-filer-block styled-scrollbar">
        <div class="container">
            <div class="row">
                <div class="col-12 my-3">
                    <form>
                        <div class="form-group">
                            <label for="expense-list-filter-date-from">Data od</label>
                            <input type="datetime-local" id="expense-list-filter-date-from" name="filter-date-from"
                                   class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="expense-list-filter-date-to">Data do</label>
                            <input type="datetime-local" id="expense-list-filter-date-to" name="filter-date-to"
                                   class="form-control">
                        </div>

                        <div class="form-group mt-5">
                            <label for="expense-list-filter-acc">Konto</label>
                            <select id="expense-list-filter-acc" name="filter-acc" class="form-control">
                                <option value="-1" selected>Brak</option>
                                <?php foreach ($variables['accounts'] as $account) : ?>
                                    <option value="<?= $account['id_m_acc'] ?>"><?= $account['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group mt-5">
                            <label for="expense-list-filter-category">Kategoria</label>
                            <select id="expense-list-filter-category" name="filter-category" class="form-control">
                                <option value="-1">Brak</option>
                                <?php foreach ($variables['categoryList'] as $category) : ?>
                                    <option value="<?= $category['id_category'] ?>"><?= $category['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <!--
                        <div class="form-group">
                            <label for="expense-list-filter-subcategory">Podategoria</label>
                            <select id="expense-list-filter-subcategory" name="filter-subcategory" class="form-control">
                                <option value="-1" selected>Brak</option>
                                <option>Jedzenie</option>
                                <option>Rachunki</option>
                            </select>
                        </div>
                        -->

                        <div class="form-group mt-5">
                            <input type="button" class="btn btn-success" id="filter-button" value="Filtruj" onclick="filterExpenses()">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</aside>

<div class="content content-with-right-aside content-top-panel">
    <h3 class="text-center" id="sum-money-listed">Suma: <?= $variables['sumMoney'] ?>zł</h3>
</div>

<div class="content content-with-right-aside styled-scrollbar content-with-top-panel">
    <div class="container">

        <div class="row mt-1">
            <div class="col-md-12 p-0" id="expense_list-container">
                <?php foreach ($variables['expenses'] as $exp) : ?>
                    <div class="item-main">
                        <div class="d-flex">
                            <div class="item-thunbail align-self-center" style="background:<?= $exp['color'] ?>">
                                <?php $t = mb_substr($exp['category'], 0, 1, 'UTF-8');
                                echo $t; ?>
                            </div>

                            <div class="item-right-part">
                                <div class="item-section-data-category">
                                    <p class="item-date"><?= $exp['date'] ?></p>
                                    <p class="item-cat-name"><?= $exp['subcategory'] ?></p>
                                    <p class="item-description"><?= $exp['note'] ?></p>
                                </div>
                                <div class="item-section-type-money">
                                    <p class="item-money-acc"><?= $exp['account'] ?></p>
                                    <?php if ($exp['amount'] > 0) : ?>
                                        <p class="item-money-amount item-money-income"><?= $exp['amount'] ?>zł</p>
                                    <?php else : ?>
                                        <p class="item-money-amount item-money-expense"><?= $exp['amount'] ?>zł</p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>


            </div>
        </div>
    </div>
</div>

<!-- Notify -->
<div class="notify-block">

    <?php require_once "Views\blocks\cookieInformationNotify.html"; ?>
</div>

<?php
require_once "Views\blocks\modals.php";
require_once "Views\blocks\bootstrapFooter.html"; ?>

<script src="Views/js/expenseList.js"></script>
</html>