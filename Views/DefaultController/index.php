<!doctype html>
<html lang="pl" class="max-window-height">
<head>
    <?php /** @var array $variables */
    require_once("Views/blocks/head.html"); ?>
    <!--suppress HtmlUnknownTarget -->
    <link rel="stylesheet" href="Views/css/login_window.css">
</head>
<body class="fushia-background">
<div class="login-form">
    <div class="login-container">
        <div class="row">
            <div class="col-md-6 p-0">
                <h2 class="text-center">Zaloguj</h2>
                <div class="login-form-part">
                    <form method="POST" class="login-form-part-content" action="?page=login">
                        <div class="form-group">
                            <label for="login-part-email">Email:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-at"></i>
                                    </div>
                                </div>
                                <input type="email" class="form-control
                                <?php
                                if ($variables['loginerror']) : ?>is-invalid <?php endif; ?>
                                " name="login-part-email" id="login-part-email"
                                       placeholder="Email adres">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="login-part-password">Hasło:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-key"></i>
                                    </div>
                                </div>
                                <input type="password" class="form-control <?php if ($variables['loginerror']) : ?>is-invalid<?php endif; ?>
                                " name="login-part-password"
                                       id="login-part-password" placeholder="Hasło">
                                <?php if ($variables['loginerror']) : ?>
                                    <div class="invalid-feedback">Błąd logowania!</div>
                                <?php endif; ?>
                            </div>

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success col-12 mt-3" value="Zaloguj">
                            <a href="#" class="text-center d-block mt-4">Zapomniałem hasła</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <h2 class="text-center">Zarejestruj</h2>
                <div class="login-form-part">
                    <form method="POST" class="login-form-part-content" id="register-form" action="?page=registerUser">
                        <div class="form-group">
                            <label for="register-part-email">Email:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-at"></i>
                                    </div>
                                </div>
                                <input type="email" class="form-control form-control-sm" id="register-part-email"
                                       name="register-part-email" placeholder="Adres email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<label for="register-part-email-repeat">Powtórz email:</label>-->
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-at"></i>
                                    </div>
                                </div>
                                <input type="email" class="form-control form-control-sm" id="register-part-email-repeat"
                                       name="register-part-email-repeat" placeholder="Powtórz adres email" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="register-part-password">Hasło:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-key"></i>
                                    </div>
                                </div>
                                <input type="password" class="form-control form-control-sm" id="register-part-password"
                                       name="register-part-password" placeholder="Hasło" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<label for="register-part-password-repeat">Powtórz hasło:</label>-->
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-key"></i>
                                    </div>
                                </div>
                                <input type="password" class="form-control form-control-sm"
                                       id="register-part-password-repeat" name="register-part-password-repeat"
                                       placeholder="Powtórz hasło" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="register-part-username">Nazwa użytkownika:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-user"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control form-control-sm" id="register-part-username"
                                       name="register-part-username" placeholder="Nazwa użytkownika" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success col-12 mt-3" value="Zarejestruj">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Notify -->
<div class="notify-block">
    <?php if($variables['delUser'] == "true") : ?>
    <div class="notify-item">
        <div class="notify-close" onclick="hideCookieInformation(this)">X</div>
        <p><strong>Twoje konto zostało usuniete!!</strong></p>
        <p>Twoje konot oraz wszystkie dane zostaly usunięte, już nie możesz się zalogować do naszego serwisu.</p>
    </div>
    <?php endif; ?>
    <?php if($variables['exist'] == "exist") : ?>
        <div class="notify-item">
            <div class="notify-close" onclick="hideCookieInformation(this)">X</div>
            <p><strong>Użytkownik o podanym emailu istnieje!!</strong></p>
            <p>Posiadamy w bazie już użytkonwika o podanym emailu.</p>
        </div>
    <script>
        const registerUserExist = 1;
    </script>
    <?php endif; ?>
    <?php require_once "Views\blocks\cookieInformationNotify.html";?>
</div>


<?php
require_once "Views\blocks\bootstrapFooter.html"; ?>
<script src="Views/js/loginPage.js"></script>
</body>
</html>