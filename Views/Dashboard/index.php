<!doctype html>
<html lang="pl" class="max-window-height">
<head>
    <?php /** @var array $variables */
    require_once("Views/blocks/head.html"); ?>
</head>
<body class="white-background max-window-height">
<?php
require_once "Views\blocks\leftmenu.html"; ?>

<div class="content styled-scrollbar">
    <div class="container">
        <div class="row">
            <div class="dashboard-background col-12">
                <div class="d-flex">
                    <div class="p-2 align-self-center">
                        <img alt="User image" src="Views/img/default_user.png" class="img-user">
                    </div>
                    <h2 class="p-3 align-self-center"><?=$variables['username'] ?></h2>
                    <h4 class="ml-auto align-self-center" id="sum-money-dashboard"><?=$variables['money'] ?> zł</h4>
                </div>
                <div class="d-flex flex-wrap justify-content-end">
                    <?php foreach($variables['accounts'] as $acc) : ?>
                    <div class="dashboard-account-preview">
                        <p><strong><?= $acc['name']?></strong></p>
                        <p><?= number_format($acc['COMON'], 2,",", " ")?> zł</p>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-12 dashboard-background">
                <h1>Ostatnie wpisy</h1>

                <div id="expense_list-container">
                <?php foreach($variables['expenses'] as $exp) : ?>
                <div class="item-main">
                    <div class="d-flex">
                        <div class="item-thunbail align-self-center" style="background:<?=$exp['color']?>">
                            <?php $t = mb_substr($exp['category'], 0, 1, 'UTF-8'); echo $t;?>
                        </div>

                        <div class="item-right-part">
                            <div class="item-section-data-category">
                                <p class="item-date"><?=$exp['date']?></p>
                                <p class="item-cat-name"><?=$exp['subcategory']?></p>
                                <p class="item-description"><?=$exp['note']?></p>
                            </div>
                            <div class="item-section-type-money">
                                <p class="item-money-acc"><?=$exp['account']?></p>
                                <?php if($exp['amount'] > 0) : ?>
                                <p class="item-money-amount item-money-income"><?=$exp['amount']?>zł</p>
                                <?php else :?>
                                <p class="item-money-amount item-money-expense"><?=$exp['amount']?>zł</p>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Notify -->
<div class="notify-block">

    <?php require_once "Views\blocks\cookieInformationNotify.html";?>
</div>

<script src="Views/js/expenseList.js"></script>
<?php
require_once "Views\blocks\modals.php";
require_once "Views\blocks\bootstrapFooter.html"; ?>
</html>