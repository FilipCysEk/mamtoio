function invalidInput(input){
    if($(input).hasClass("is-valid"))
        $(input).removeClass("is-valid");

    if(! $(input).hasClass("is-invalid"))
        $(input).addClass("is-invalid");

}

function invalidInputTxt(input, text){
    invalidInput(input);

    let nextElem = $(input).next().get();
    if(nextElem != null){
        if($(nextElem).is("div")) {
            if ($(nextElem).hasClass("invalid-feedback")) {
                $(nextElem).text(text);
                return true;
            }
        }
    }

    $(input).after($("<div></div>").addClass("invalid-feedback").text(text));
}

function validInput(input){
    if($(input).hasClass("is-invalid"))
        $(input).removeClass("is-invalid");

    if(! $(input).hasClass("is-valid"))
        $(input).addClass("is-valid");

}

function validInputTxt(input, text){
    validInput(input);

    let nextElem = $(input).next().get();
    if(nextElem != null){
        if($(nextElem).is("div")) {
            if ($(nextElem).hasClass("valid-feedback")) {
                $(nextElem).text(text);
                return true;
            }
        }
    }

    $(input).after($("<div></div>").addClass("valid-feedback").text(text));
}

function deleteInputFeedbackTxt(input){
    let nextElem = $(input).next().get();
    if(nextElem != null){
        if($(nextElem).is("div")) {
            if ($(nextElem).hasClass("form-control-feedback")) {
                $(nextElem).remove();
            }
        }
    }
}

function deleteInputFeedback(input){
    if($(input).hasClass("is-valid"))
        $(input).removeClass("is-valid");

    if($(input).hasClass("is-invalid"))
        $(input).removeClass("is-invalid");

    deleteInputFeedbackTxt(input);
}

function isInPasswordBigLetters(pass, emount){
    let bigLetters = pass.length - pass.replace(/[A-Z]/, "").length;
    console.log(bigLetters);
    return bigLetters >= emount;
}

function isInPasswordNumbers(pass, emount){
    let bigLetters = pass.length - pass.replace(/[0-9]/, "").length;
    console.log(bigLetters);
    return bigLetters >= emount;
}

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

