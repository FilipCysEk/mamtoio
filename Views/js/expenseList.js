//$("#filter-button").click(filterExpenses);

function filterExpenses() {
    let dateFrom = $("#expense-list-filter-date-from").get();
    let dateTo = $("#expense-list-filter-date-to").get();
    let account = $("#expense-list-filter-acc").get();
    let category = $("#expense-list-filter-category").get();

    if ($(dateFrom).val() > $(dateTo).val()) {
        invalidInputTxt(dateFrom, "Data od powinna być mniejsza od daty do");
        return false;
    } else
        deleteInputFeedback(dateFrom);

    if ((Number($(dateFrom).val() == "") + Number($(dateTo).val() == "")) == 1) {
        invalidInputTxt(dateFrom, "Musisz podać obydwie daty, albo wogóle.");
        return false;
    }

    //if($(dateFrom).val() == "" && $(dateTo).val() == "" && $(account).val() == -1 && $(category).val() == -1)
    //   return false;

    dateFrom = $(dateFrom).val() == "" ? null : $(dateFrom).val();
    dateTo = $(dateTo).val() == "" ? null : $(dateTo).val();

    account = $(account).val() == -1 ? null : $(account).val();
    category = $(category).val() == -1 ? null : $(category).val();

    let resTable = executeFiltering(dateFrom, dateTo, account, category);


}

function getLast10Items() {
    $.ajax({
        type: "POST",
        url: "?page=ajaxGetLast10Item",
        success: function (json) {
            let container = $('#expense_list-container').get();

            renderExpenseList(container, json);
        },
        error: function (json) {
            alert("Error");
            console.log("Error when executing in DB filtering Expenses");
            console.log(json);
        }
    });
}

function executeFiltering(dateFrom, dateTo, account, category) {
    $.ajax({
        type: "POST",
        url: "?page=ajaxFilterExpenses",
        data: {
            dateFrom: dateFrom,
            dateTo: dateTo,
            account: account,
            category: category
        },
        success: function (json) {
            let container = $('#expense_list-container').get();
            renderExpenseList(container, json);
        },
        error: function (json) {
            alert("Error");
            console.log("Error when executing in DB filtering Expenses");
            console.log(json);
        }
    });
}

function renderExpenseList(container, res) {
    let temp;
    let main;
    let submain;
    let sum = 0;

    $(container).empty();

    let size = res.length;
    for (let i = 0; i < size; i++) {
        main = $("<div></div>").addClass("item-main");
        $(container).append(main);

        temp = $("<div></div>").addClass("d-flex");
        $(main).append(temp);

        main = temp;
        //Thunbail
        temp = $("<div></div>").addClass("item-thunbail align-self-center");
        $(temp).css("background", res[i]['color']);
        $(temp).text(res[i]['category'][0]);
        $(main).append(temp);

        //right part
        temp = $("<div></div>").addClass("item-right-part");
        $(main).append(temp);

        main = temp;

        //Item section name, note date
        temp = $("<div></div>").addClass("item-section-data-category");
        $(main).append(temp);
        submain = temp;

        //Item date
        temp = $("<p></p>").addClass("item-date");
        $(temp).text(res[i]['date']);
        $(submain).append(temp);

        //Item name
        temp = $("<p></p>").addClass("item-cat-name");
        $(temp).text(res[i]['subcategory']);
        $(submain).append(temp);

        //Item note
        temp = $("<p></p>").addClass("item-description");
        $(temp).html(res[i]['note']);
        $(submain).append(temp);

        //Item section type money
        temp = $("<div></div>").addClass("item-section-type-money");
        $(main).append(temp);
        submain = temp;

        //Item note
        temp = $("<p></p>").addClass("item-money-acc");
        $(temp).text(res[i]['account']);
        $(submain).append(temp);

        //Item money
        if (res[i]['amount'] > 0)
            temp = $("<p></p>").addClass("item-money-amount item-money-income");
        else
            temp = $("<p></p>").addClass("item-money-amount item-money-expense");

        $(temp).text(res[i]['amount'] + "zł");
        $(submain).append(temp);

        sum += Number(res[i]['amount']);
    }

    sum = Math.round(sum * 100) / 100;
    $('#sum-money-listed').text("Suma: " + sum + "zł");
    $('#sum-money-dashboard').text(sum + "zł");
}